//
//  ViewController.swift
//  GuessTheFlag
//
//  Created by Johan Graucob on 2015-11-05.
//  Copyright © 2015 Johan Graucob. All rights reserved.
//

import UIKit
import GameplayKit

class EasyViewController: UIViewController {

    @IBOutlet weak var button1: UIButton!
    @IBOutlet weak var button2: UIButton!
    @IBOutlet weak var button3: UIButton!
    @IBOutlet weak var scoreLabel: UILabel!
    
    // the () initializes the array
    var emotes = [String]()
    var score = 0
    var correctAnswer = 0
    
 
    
    override func viewDidLoad() {
        super.viewDidLoad()
        emotes += ["ANELE", "deIlluminati", "duDudu", "EleGiggle", "HeyGuys", "KappaPride", "KappaRoss", "MingLee", "NotLikeThis", "PunchTrees", "WutFace", "Kappa"]
        
        
        
        
        askQuestion()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func askQuestion(){
        //randomize order of flags in array
        emotes = GKRandomSource.sharedRandom().arrayByShufflingObjectsInArray(emotes) as! [String]
        
        // sets button images
        button1.setImage(UIImage(named: emotes[0]), forState: .Normal)
        button2.setImage(UIImage(named: emotes[1]), forState: .Normal)
        button3.setImage(UIImage(named: emotes[2]), forState: .Normal)
        correctAnswer = GKRandomSource.sharedRandom().nextIntWithUpperBound(3)
        
        title = emotes[correctAnswer]
    }
    
    
    @IBAction func buttonTapped(sender: UIButton) {
        
        
        UIView.animateWithDuration(0.2, animations: { () -> Void in
            
            sender.transform = CGAffineTransformMakeScale(2,2)
            
        })
        
        sender.transform = CGAffineTransformMakeScale(1, 1)
        
        UIView.animateWithDuration(0.2, animations: { () -> Void in
            
            sender.transform = CGAffineTransformMakeScale(1,1)
            
        })
        
        if(sender.tag == correctAnswer){
            ++score

            scoreLabel.text = "\(score)"
            askQuestion()
            
        }else{
            score = 0
            scoreLabel.text = "\(score)"
            askQuestion()
        }
        
    }
 

}

