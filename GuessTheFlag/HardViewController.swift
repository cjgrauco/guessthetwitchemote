//
//  HardViewController.swift
//  GuessTheFlag
//
//  Created by Johan Graucob on 2015-11-08.
//  Copyright © 2015 Johan Graucob. All rights reserved.
//

import UIKit

class HardViewController: UIViewController {

    @IBOutlet weak var emoteImage: UIImageView!
    @IBOutlet weak var answerTextField: UITextField!
    
    var emotes = [String]()
    var score = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        emotes += ["ANELE_big", "deIlluminiati_big", "duDudu_big"]
    }
    
}